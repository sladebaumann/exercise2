<!DOCTYPE html>
<html>
<head>
	<title>Basic Blog - Post</title>
	<link rel="stylesheet"

href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('posts') }}">Posts Page</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('posts') }}">View All Posts</a></li>
		<li><a href="{{ URL::to('posts/create') }}">Create a Post</a>
		<!-- LOGOUT BUTTON -->
        <li><a href="{{ URL::to('logout') }}">Logout</a></li>
	</ul>
</nav>

<h1>Create a Post</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'posts')) }}

	<div class="form-group">
		{{ Form::label('title', 'Title') }}
		{{ Form::text('title', Input::old('title'), array('class' => 'form-

control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('post', 'Post') }}
		{{ Form::textarea('post', Input::old('post'), array('class' => 'form-

control')) }}
	</div>


	{{ Form::submit('Create the Post!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>